#include <algorithm>
#include <iostream>
#include <climits>
#include <vector>
using std::vector;

struct Segment {
  int start, end;
};

bool acompare(Segment lhs, Segment rhs) {return lhs.end < rhs.end; }

vector<int> optimal_points(vector<Segment> &segments) {
    vector<int> points;
    int point = -1;
    
    // Sort segment based on end
    std::sort(segments.begin(), segments.end(), acompare);
    for (size_t i = 0; i < segments.size(); ++i) {
        if (point < segments[i].start) {
            points.push_back(segments[i].end);
            point = segments[i].end;
        } else {
            continue;
        }
    }
    return points;
}

int main() {
    int n;
    
    std::cin >> n;
    vector<Segment> segments(n);
    
    for (size_t i = 0; i < segments.size(); ++i) {
      std::cin >> segments[i].start >> segments[i].end;
    }
    
    vector<int> points = optimal_points(segments);
    std::cout << points.size() << "\n";
    
    for (size_t i = 0; i < points.size(); ++i) {
      std::cout << points[i] << " ";
    }
}