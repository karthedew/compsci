#include <iostream>
#include <vector>

using std::vector;


long long get_fibonacci_partial_sum_naive(long long from, long long to) {
    
    long long sum = 0;
    long long current = 0;
    long long next  = 1;

    for (long long i = 0; i <= to; ++i) {
        if (i >= from) {
            sum += current;
        }
        long long new_current = next;
        next = next + current;
        current = new_current;
    }

    return sum % 10;
}


long long get_fibonacci_partial_sum_fast(long long from, long long to) {

    long long next, current, sum;
    
    // The 60 is a cycle for the last digit of fibonacci numbers.
    long long fromLen = from % 60;
    long long toLen = to % 60;

    if (from > to) {
        from = toLen;
        to = fromLen;
    } else {
        from = fromLen;
        to = toLen;
    }

    sum = 0;
    current = 0;
    next = 1;

    for (long long i = 0; i <= to; ++i) {
        
        if (i >= from) {
            sum += current;
        }

        long long new_current = (next + current) % 10;
        next = current;
        current = new_current;
        
    }
   
    return sum % 10;

}


int main() {
    long long from, to, nm;
    std::cin >> from >> to;
    std::cout << get_fibonacci_partial_sum_fast(from, to) << '\n';

    std::cin >> nm;
}