#include <iostream>

long long lcm_naive(int a, int b) {
  for (long l = 1; l <= (long long) a * b; ++l)
    if (l % a == 0 && l % b == 0)
      return l;
  return (long long) a * b;
}

int gcd_euclidean(int a, int b) {

    if (b == 0) {
        return a;
    } else {
        gcd_euclidean(b, a%b);
    }
}

long long lcm_fast(int a, int b) {
    int gcd = 0;
    gcd = gcd_euclidean(a, b);

    return (long long) a*b/gcd;
}

int main() {
  int a, b;
  std::cin >> a >> b;
  //std::cout << lcm_naive(a, b) << std::endl;
  std::cout << lcm_fast(a, b) << std::endl;
  return 0;
}