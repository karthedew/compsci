#include <iostream>
long long get_pisano_number(long long m) {
    long long a = 0;
    long long b = 1;
    long long sum = m*m;
 
    for (long long i = 0; i < sum; ++i) {
        
        long long temp = (a + b) % m;
        a = b;
        b = temp;
        
        if (a == 0 && b == 1) {
            return i + 1;
        }
    } 
}

long long get_fibonacci_huge_fast(long long n, long long m) {
    long long previous, current;
    
    if (n <= 1) return n;
 
    long long modLen = n % get_pisano_number(m);
    if (modLen == 0) {
        current = 0;
    } else {
        previous = 0;
        current = 1;
    }
    for (long long i = 0; i < modLen - 1; ++i) {
        long long tmp_previous = (previous + current) % m;
        previous = current;
        current = tmp_previous;
    }
   
    return current % m;
}

int main() {
    long long n, m;
    std::cin >> n >> m;
    std::cout << get_fibonacci_huge_fast(n, m) << '\n';
}