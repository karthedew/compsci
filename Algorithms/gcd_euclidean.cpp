#include <iostream>

/**
For integers, a and b, their greatest common divisor (or gcd(a,b))
is the largest integer d so that d divides both a and b.

     Input:   Integers a,b >= 0
    Output:   gcd(a,b)
*/

int gcd_naive(int a, int b) {
  int current_gcd = 1;
  for (int d = 2; d <= a && d <= b; d++) {
    if (a % d == 0 && b % d == 0) {
      if (d > current_gcd) {
        current_gcd = d;
      }
    }
  }
  return current_gcd;
}


int gcd_euclidean(int a, int b) {

    if (b == 0) {
        return a;
    } else {
        gcd_euclidean(b, a%b);
    }
}


int main() {

    int a,b;

    std::cin >> a;
    std::cin >> b;

    std::cout << gcd_euclidean(a,b) << std::endl;
    return 0;
}