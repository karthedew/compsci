#include <iostream>

int fibonacci_sum_naive(long long n) {
    if (n <= 1)
        return n;
    long long previous = 0;
    long long current  = 1;
    long long sum      = 1;
    for (long long i = 0; i < n - 1; ++i) {
        long long tmp_previous = previous;
        previous = current;
        current = tmp_previous + current;
        sum += current;
    }
    return sum % 10;
}

long long fibonacci_sum_fast(long long n) {
    long long previous, current, sum;
    
    if (n <= 1) return n;
 
    long long modLen = n % 60;
    if (modLen == 0) {
        current = 0;
        sum = 0;
    } else {
        previous = 0;
        current = 1;
        sum = 1;
    }
    for (long long i = 0; i < modLen - 1; ++i) {
        long long tmp_previous = (previous + current) % 10;
        previous = current;
        current = tmp_previous;
        sum += current;
    }
   
    return sum % 10;

}

int main() {
    long long n = 0;
    std::cin >> n;
    //std::cout << fib_sum60() << std::endl;
    std::cout << fibonacci_sum_fast(n) << std::endl;
}