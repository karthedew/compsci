#include <iostream>
#include <vector>
#include <unordered_set>

using std::vector;

vector<int> optimal_summands(int n) {
  vector<int> summands;
  std::unordered_set<int> intSet;
  int adder = 1;
  int rlt = 0;

  // This loop 
  while (n != 0) {
      rlt = n-adder;
      adder += 1;
      // Handle if difference is one
      if ( rlt == 1 ) {
          intSet.insert(adder);
          summands.push_back(adder);
          n = n - adder;
        // If difference is a number previously seen, add the current adder and return
      } else if (intSet.find(n-adder) != intSet.end()) {
          // this is found
          summands.push_back(n);
          n = 0;
        // Add the new number if difference doesn't consist of a number previously seen
      } else {
          intSet.insert(adder-1);
          summands.push_back(adder-1);
          n = rlt;
      }

  }
  return summands;
}


int main() {

  int n;
  std::cin >> n;
  vector<int> summands = optimal_summands(n);
  std::cout << summands.size() << '\n';
  for (size_t i = 0; i < summands.size(); ++i) {
    std::cout << summands[i] << ' ';
  }
  return 0;
}