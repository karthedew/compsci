#include <iostream>
#include <vector>

using std::vector;

double get_optimal_value(double capacity, vector<double> weights, vector<double> values) {
  double value = 0.0;

  // write your code here
  while (capacity != 0 && weights.size() > 0) {
      // put your info here
      double max = 0.0;
      int maxIndex = 0;
      
      // Loop to find max price index
      for (std::size_t i=0; i < weights.size(); ++i) {
          double price = values[i] / weights[i];
          if (price > max) {
              max = price;
              maxIndex = i;
          }
      }

      //Check to see what the capacity level is left
      if (weights[maxIndex] <= capacity) {
          value = value + values[maxIndex];         // add the full weight
          capacity = capacity - weights[maxIndex];  // set the new capacity

          // Remove the current price
          weights.erase(weights.begin()+maxIndex);
          values.erase(values.begin()+maxIndex);
      } else {
          // add the fractional weight
          value = value + (values[maxIndex] / (weights[maxIndex] / capacity));
          capacity = capacity - weights[maxIndex] / (weights[maxIndex] / capacity);

          // Remove the current price
          weights.erase(weights.begin()+maxIndex);
          values.erase(values.begin()+maxIndex);
      }

      
  }
  return value;
}


int main() {
  int n;
  double capacity;
  std::cin >> n >> capacity;
  vector<double> values(n);
  vector<double> weights(n);
  for (int i = 0; i < n; i++) {
    std::cin >> values[i] >> weights[i];
  }
  double optimal_value = get_optimal_value(capacity, weights, values);
  std::cout.precision(10);
  std::cout << optimal_value << std::endl;
  return 0;
}
